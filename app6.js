function createNewUser() {
    let x = prompt('Enter your First Name');
    while (!x) {
        x = prompt('Enter your First Name');
    }

    let y = prompt('Enter your Last Name');
    while (!y) {
        y = prompt('Enter ypur Last Name');
    }

    let b = prompt('Enter your Date of Birth (dd.mm.yyyy)')
    let newUser = {
        firstName: x,
        lastName: y,
        birthday: b,
        getLogin: function () {
            return(this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function() {
            let splitTheDate = b.split('.');
            let currentDate = new Date();
            let userDate = new Date(splitTheDate[2], splitTheDate[1]-1, splitTheDate[0]);
            return currentDate.getFullYear()-userDate.getFullYear();
        },
        getPassword: function() {
            let splitTheDate = b.split('.');
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + splitTheDate[2];
        }
    }
    return(newUser);
}
let user = createNewUser(); 
console.log(user.getLogin());  
console.log(user.getAge());
console.log(user.getPassword());     